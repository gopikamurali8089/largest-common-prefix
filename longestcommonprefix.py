def commonprefix(a):
    if not a:
        return ""
    size=len(a)
    a=sorted(a)
    length=min(len(a[0]),len(a[size-1]))
    i=0
    while(i<length and a[0][i]==a[size-1][i]):
         i=i+1
    prefix=a[0][0:i]
    return prefix
number=int(input("enter the number of strings"))   
strings=[]
for i in range(number):
    user_input=input("enter the string")
    strings.append(user_input)
print(commonprefix(strings))